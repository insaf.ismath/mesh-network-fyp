//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

package brokerless;

simple Sensor
{
    parameters:

    gates:
        inout io;

}

simple Queue
{
    parameters:
        int queue_length = default(32);
    gates:
        inout to_sensor;
        inout to_zigbee;
}


simple Zigbee
{
    parameters:
        //PL=PLd0+10*pathLossExponent*log(d/d0)+Xg  log-distance pathloss model-flat fading
        double pathLossExponent @unit(dB) = default(3.5dB);	// how fast is the signal strength fading(2.7-3.5)
        double PLd0 @unit(dB) = default(105.6dB);					// path loss at reference distance d0 (in dBm)
        double d0 @unit(m) = default(142.8m);					// reference distance d0 (in meters)
        double sigma @unit(dB) = default(6.25dB);
        double noisepower @unit(dBm) = default(-92.0dBm); //environmental noise around 2.4GHz
        double SNRmin @unit(dB) = default(3dB); //minimum  SNR for PER 0%

        //XBEE Pro Specification
        //TX power can be 10,12,14,16,18 dBm
        //10dBm=>171.53,12dBm=>203.43,14dBm=>241.27,16dBm=>286.14m,18dBm=>339.35m
        double Tx_power @unit(dBm) = default(18dBm);
        double datarate @unit(kbps) = default(250.0kbps);
        double antenna_gain @unit(dB) = default(2dB);
        double rec_sensitivity @unit(dBm) = default(-100dBm);


    gates:
        inout node_io;
        input wireless @directIn;
}

module Node
{
    parameters:
        @display("i=device/device");
    submodules:
        zigbee: Zigbee {
            @display("i=device/antennatower;p=58,200");
        }
        sensor: Sensor {
            @display("i=device/device;p=58,57");

        }

        queue: Queue {
            @display("i=block/queue;p=58,126");
        }

    connections:
        //        sensor.io <--> zigbee.node_io;
        sensor.io <--> queue.to_sensor;
        queue.to_zigbee <--> zigbee.node_io;
}
