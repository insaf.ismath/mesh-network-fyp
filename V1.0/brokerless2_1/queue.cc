/*
 * queue.cc
 *
 *  Created on: Jan 16, 2018
 *      Author: Arun Kumar
 */

#include <omnetpp.h>
#include <cqueue.h>
#include <string.h>
#include "message_m.h"
#include "data_message_m.h"
#include "routing_message_m.h"


class Queue : public cSimpleModule
{
    private:
        int neighbour_sensorID;
        int neighbour_zigbeeID;

        long rec_Packet;
        long drop_Packet;

        double rank = 1000;

        double energyForThisHop;
        int parentID;
        int NodeID;



    protected:
        virtual void initialize();
        virtual void handleMessage(cMessage *msg);
        virtual void finish();
        virtual double generateRank(double rank);
        virtual double getEnergyForThisHop();

};
Define_Module(Queue);

void Queue::initialize()
{
    // TODO - Generated method body

    NodeID=this->getParentModule()->getIndex();
    neighbour_sensorID = gate("to_sensor$o")->getNextGate()->getOwnerModule()->getId();
    neighbour_zigbeeID = gate("to_zigbee$o")->getNextGate()->getOwnerModule()->getId();


    rec_Packet=0;
    WATCH(rec_Packet);
    drop_Packet=0;
    WATCH(drop_Packet);

}

double Queue::generateRank(double senderRank)
{   double updatedRank = senderRank + 1;
    return updatedRank;
}
double Queue::getEnergyForThisHop()
{   double energyForThisHop = 25;
    return energyForThisHop;
}
void Queue::handleMessage(cMessage *msg)
{
    if(strcmp(msg->getClassName(),"Routing_message")==0){

        Routing_message *in_msg = check_and_cast<Routing_message *>(msg);
        double senderRank = in_msg->getRank();
        int senderID=in_msg->getSenderModule()->getId();


        if(senderRank+1 < rank){
            rank = generateRank(senderRank);
            parentID = in_msg->getSourceID();
//            simulation.getModule(senderID)->getParentModule()->bubble("Rank Updated!");

            char buffer [50];
            sprintf (buffer, "Rank = %f ", rank);

            simulation.getModule(senderID)->getParentModule()->bubble(buffer);


            sprintf (buffer, "Routing_Message: %f ", rank);

            Routing_message *out_msg = new Routing_message(buffer);
            out_msg->setRank(rank);
            out_msg->setSourceID(NodeID);
            out_msg->setConcentratorID(in_msg->getConcentratorID());
            out_msg->setTotalHopEnergy(in_msg->getTotalHopEnergy()+getEnergyForThisHop());
            out_msg->setNoHopsToConcentrator(in_msg->getNoHopsToConcentrator() + 1);
            send(out_msg->dup(),"to_zigbee$o");
            send(out_msg,"to_sensor$o");

        }
    }
    else if(strcmp(msg->getClassName(),"Data_message")==0){

        Data_message *in_msg = check_and_cast<Data_message *>(msg);

        int senderID=in_msg->getSenderModule()->getId();

        if(senderID==neighbour_sensorID){
            in_msg->setDestinationID(parentID);
            send(in_msg,"to_zigbee$o");
        }
        else if(in_msg->getDestinationID()==NodeID)
        {
            in_msg->setDestinationID(parentID);
            send(in_msg,"to_zigbee$o");

        }

    }


//    if(strcmp(msg->getClassName(),"Message")==0){
//        Message *rec_msg = check_and_cast<Message *>(msg);
//        int senderID=rec_msg->getSenderModule()->getId();
//
//
//        if(rec_msg==pop){
//            if(!queue.empty()){
//            cMessage* queue_msg = (Message *)queue.pop();
//            send(queue_msg,"to_sensor$o");
//            pop_scheduled=false;
//            }
//
//        }
//
//
//        // pass the message from application layer to physical layer
//        else if(senderID==neighbour_sensorID){
//            send(rec_msg,"to_zigbee$o");
//        }
//
//        // handle the messages from physical layer
//        else if(senderID==neighbour_zigbeeID){
//
//            rec_Packet++;
//
//            if(queue.length()<queue_length){
//                queue.insert(rec_msg);
//            }
//
//            else{
//                simulation.getModule(senderID)->getParentModule()->bubble("packet dropped");
//                drop_Packet++;
//
//            }
//
//        }
//    }
}

void Queue::finish(){
    recordScalar("#received",rec_Packet);
    recordScalar("#dropped",drop_Packet);
    EV<<rec_Packet<<"@@@"<<drop_Packet;
}











