//
// Generated file, do not edit! Created by nedtool 4.6 from data_message.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "data_message_m.h"

USING_NAMESPACE


// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




// Template rule for outputting std::vector<T> types
template<typename T, typename A>
inline std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec)
{
    out.put('{');
    for(typename std::vector<T,A>::const_iterator it = vec.begin(); it != vec.end(); ++it)
    {
        if (it != vec.begin()) {
            out.put(','); out.put(' ');
        }
        out << *it;
    }
    out.put('}');
    
    char buf[32];
    sprintf(buf, " (size=%u)", (unsigned int)vec.size());
    out.write(buf, strlen(buf));
    return out;
}

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
inline std::ostream& operator<<(std::ostream& out,const T&) {return out;}

Register_Class(Data_message);

Data_message::Data_message(const char *name, int kind) : ::cPacket(name,kind)
{
    this->reading_var = 0;
    this->rank_var = 0;
    this->destinationID_var = 0;
    this->sourceID_var = 0;
}

Data_message::Data_message(const Data_message& other) : ::cPacket(other)
{
    copy(other);
}

Data_message::~Data_message()
{
}

Data_message& Data_message::operator=(const Data_message& other)
{
    if (this==&other) return *this;
    ::cPacket::operator=(other);
    copy(other);
    return *this;
}

void Data_message::copy(const Data_message& other)
{
    this->reading_var = other.reading_var;
    this->rank_var = other.rank_var;
    this->destinationID_var = other.destinationID_var;
    this->sourceID_var = other.sourceID_var;
}

void Data_message::parsimPack(cCommBuffer *b)
{
    ::cPacket::parsimPack(b);
    doPacking(b,this->reading_var);
    doPacking(b,this->rank_var);
    doPacking(b,this->destinationID_var);
    doPacking(b,this->sourceID_var);
}

void Data_message::parsimUnpack(cCommBuffer *b)
{
    ::cPacket::parsimUnpack(b);
    doUnpacking(b,this->reading_var);
    doUnpacking(b,this->rank_var);
    doUnpacking(b,this->destinationID_var);
    doUnpacking(b,this->sourceID_var);
}

double Data_message::getReading() const
{
    return reading_var;
}

void Data_message::setReading(double reading)
{
    this->reading_var = reading;
}

double Data_message::getRank() const
{
    return rank_var;
}

void Data_message::setRank(double rank)
{
    this->rank_var = rank;
}

int Data_message::getDestinationID() const
{
    return destinationID_var;
}

void Data_message::setDestinationID(int destinationID)
{
    this->destinationID_var = destinationID;
}

int Data_message::getSourceID() const
{
    return sourceID_var;
}

void Data_message::setSourceID(int sourceID)
{
    this->sourceID_var = sourceID;
}

class Data_messageDescriptor : public cClassDescriptor
{
  public:
    Data_messageDescriptor();
    virtual ~Data_messageDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(Data_messageDescriptor);

Data_messageDescriptor::Data_messageDescriptor() : cClassDescriptor("Data_message", "cPacket")
{
}

Data_messageDescriptor::~Data_messageDescriptor()
{
}

bool Data_messageDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<Data_message *>(obj)!=NULL;
}

const char *Data_messageDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int Data_messageDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 4+basedesc->getFieldCount(object) : 4;
}

unsigned int Data_messageDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<4) ? fieldTypeFlags[field] : 0;
}

const char *Data_messageDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "reading",
        "rank",
        "destinationID",
        "sourceID",
    };
    return (field>=0 && field<4) ? fieldNames[field] : NULL;
}

int Data_messageDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='r' && strcmp(fieldName, "reading")==0) return base+0;
    if (fieldName[0]=='r' && strcmp(fieldName, "rank")==0) return base+1;
    if (fieldName[0]=='d' && strcmp(fieldName, "destinationID")==0) return base+2;
    if (fieldName[0]=='s' && strcmp(fieldName, "sourceID")==0) return base+3;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *Data_messageDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "double",
        "double",
        "int",
        "int",
    };
    return (field>=0 && field<4) ? fieldTypeStrings[field] : NULL;
}

const char *Data_messageDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int Data_messageDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    Data_message *pp = (Data_message *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string Data_messageDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    Data_message *pp = (Data_message *)object; (void)pp;
    switch (field) {
        case 0: return double2string(pp->getReading());
        case 1: return double2string(pp->getRank());
        case 2: return long2string(pp->getDestinationID());
        case 3: return long2string(pp->getSourceID());
        default: return "";
    }
}

bool Data_messageDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    Data_message *pp = (Data_message *)object; (void)pp;
    switch (field) {
        case 0: pp->setReading(string2double(value)); return true;
        case 1: pp->setRank(string2double(value)); return true;
        case 2: pp->setDestinationID(string2long(value)); return true;
        case 3: pp->setSourceID(string2long(value)); return true;
        default: return false;
    }
}

const char *Data_messageDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    };
}

void *Data_messageDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    Data_message *pp = (Data_message *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}


