/*
 * sensor.cc
 *
 *  Created on: Dec 28, 2017
 *      Author: Arun Kumar
 */
#include <omnetpp.h>
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <map>
#include "message_m.h"
#include "data_message_m.h"
#include "routing_message_m.h"

using namespace std;

class Sensor : public cSimpleModule
{
    private:
        int SourceID;
        int concentratorID;
        int NodeID;
        int PacketID;

        double rank = 1000;
        double reading = 0;
        bool recievedTriggerMessage = false;

        double time_interval = exponential(15);

        vector<string> data_rec;
        vector<string> sub_topic;
        vector<string> key;
        map<string,int> cost;
        map<string,int> parent;
        map<string,vector<int>> child;



        cOutVector end_delay;
        int connected=0;

        Message *triggermsg;

        Message *selftest;

        simtime_t response_time;
        simtime_t initialize_time;
        simtime_t connectable_time=100;

    public:


    protected:
        virtual void initialize();
        virtual void handleMessage(cMessage *msg);
        virtual void publish();
        virtual void finish();
        virtual double getReading();

        simtime_t getNextTransmissionTime();
};

Define_Module(Sensor);

double Sensor::getReading()
{
    reading = reading + exponential(15);
    return reading;
}

void Sensor::initialize()
{
    // TODO - Generated method body
    NodeID=this->getParentModule()->getIndex();
    SourceID =this->getParentModule()->getSubmodule("zigbee")->getId();

    triggermsg = new Message();

}


void Sensor::handleMessage(cMessage *msg)
{

    if(strcmp(msg->getClassName(),"Routing_message")==0)
    {
        if(!recievedTriggerMessage){
            Routing_message *in_msg = check_and_cast<Routing_message *>(msg);
            rank = in_msg->getRank();
            concentratorID = in_msg->getConcentratorID();
            recievedTriggerMessage = true;
            scheduleAt(simTime()+time_interval,triggermsg);

        }
    }



    else if(strcmp(msg->getClassName(),"Message")==0)
    {
        Message *rec_msg = check_and_cast<Message *>(msg);
        if(rec_msg==triggermsg){

            Data_message *data_out = new Data_message("Data_Message");
            data_out->setRank(rank);
            data_out->setReading(getReading());
            data_out->setDestinationID(concentratorID);
            data_out->setSourceID(NodeID);

            send(data_out,"io$o");


            scheduleAt(simTime()+time_interval,triggermsg);

        }



    }

//    else if(strcmp(msg->getClassName(),"Tree")==0)
//    {
//        Tree *rec_msg = check_and_cast<Tree *>(msg);
//        if(rec_msg==tree_start_trigger){
//            treestarter();
//
//
//
//        }
//
//
//        else{
//
//            string rec_topic=rec_msg->getTopic();
//            int second=rec_topic.find("/",9);
//
//            string topic_check=rec_topic.substr(0,8)+"*"+rec_topic.substr(second);
//            string SubTop(SubscriptionTopic);
//            if(strcmp(rec_msg->getType(),"TreeMsg")==0 && subscriber){// && rec_topic.substr(0,second)!=my_topic.substr(0,second)){
//
//                EV<<topic_check<<"\n";
//                EV<<SubscriptionTopic<<"\n";
//                EV<<SubTop.substr(0,7)<<"@@"<<SubTop.substr(4)<<"\n";
//                EV<<topic_check.substr(0,7)<<"@@"<<topic_check.substr(10)<<"\n";
//                //////////////////////////////////////////////////////////////////////////////////////////////
//                int cost_increment;
//                if(SubTop.substr(0,7)==topic_check.substr(0,7)){
//                    cost_increment=1;
//                }
//                else if(SubTop.substr(4)==topic_check.substr(10)){
//                    cost_increment=3;
//                }
//                else{//not interested
//                    if(!publisher) {
//                        cost_increment=50;
//                    }
//                    else {
//                        cost_increment=200;
//                    }
//                }
//                ///////////////////////////////////////////////////////////////////////////////////////////////
//
//                if(find(key.begin(), key.end(),rec_topic) == key.end()){
//                    key.push_back(rec_topic);
//                    cost[rec_topic]=rec_msg->getCost();
//                    if(cost_increment<=3){
//                        //becomechild
//                        becomechild(rec_topic,-1,simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex());
//                    }
//                    //update parent table
//                    parent[rec_topic]=simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex();
//
//                    //broadcast
//                    broadcast(rec_topic,rec_msg->getCost()+cost_increment+child[rec_topic].size());
//
////                    char temp[20];
////                    sprintf(temp,"%d",cost[rec_topic]+cost_increment+child[rec_topic].size());
////                    this->getParentModule()->getDisplayString().setTagArg("t",0,temp);
//
//
//                }
//                else{
//                    if(rec_msg->getCost() < cost[rec_topic]){
//                        //EV<<"running\n";
//                        //short route
//                        cost[rec_topic]=rec_msg->getCost();//+cost_increment+child[rec_topic].size();
////                        char temp[20];
////                        sprintf(temp,"%d",cost[rec_topic]+cost_increment+child[rec_topic].size());
////                        this->getParentModule()->getDisplayString().setTagArg("t",0,temp);
//
//                        //becomechild
//                        if(cost_increment<=3 && (parent[rec_topic]!=simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex())){
//                            becomechild(rec_topic,parent[rec_topic],simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex());
//                        }
//
//                        //update parent table
//                        parent[rec_topic]=simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex();
//
//                        //broadcast
//                        broadcast(rec_topic,rec_msg->getCost()+cost_increment+child[rec_topic].size());
//
//
//                    }
//                }
//            }
//
//            else if(strcmp(rec_msg->getType(),"ChildMsg")==0){
//                //if(NodeID==96 && simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex()==26)   test=1;
//                if(!(SubTop.substr(0,7)==topic_check.substr(0,7) || SubTop.substr(4)==topic_check.substr(10)) && subscriber && child[rec_topic].size()==0){
//                    becomechild(rec_topic,-1,parent[rec_topic]);
//                }
//
//                //update child table (Add to child)
//
//                if(find(child[rec_topic].begin(), child[rec_topic].end(),simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex()) == child[rec_topic].end()){
//                    child[rec_topic].push_back(simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex());
//                }
//
//            }
//
//            else if(strcmp(rec_msg->getType(),"NoChildMsg")==0){
//                //update child table (remove from child)
//                int val=simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex();
//                child[rec_topic].erase(std::remove(child[rec_topic].begin(), child[rec_topic].end(), val), child[rec_topic].end());
//                if(!(SubTop.substr(0,7)==topic_check.substr(0,7) || SubTop.substr(4)==topic_check.substr(10)) && subscriber && child[rec_topic].size()==0){ //interested and subscribers
//                    becomechild(rec_topic,parent[rec_topic],-1);
//
//                }
//
//            }
//        }
//    }
}

void Sensor::publish(){
//    Message *msg = new Message("DATA MESSAGE");
//        PacketID=intuniform(0,1000);
//        int topic_sel=intuniform(0,2);
//        char data_type[20];
//        switch(topic_sel){
//        case(0):strcpy(data_type,"Temperature");
//        break;
//        case(1):strcpy(data_type,"Humidity");
//        break;
//        case(2):strcpy(data_type,"Light_Intensity");
//        break;
//        }
//        char topic[80];
//        sprintf(topic,"Area-%d/client-%d/",AreaID,NodeID);
//        strcat(topic,data_type);
//
//        msg->setType("DATA");
//        msg->setPacketID(PacketID);
//        msg->setTopic(topic);
//        msg->setData("#######");
//        msg->setSourceID(SourceID);
//        msg->setTimestamp();
//
//        string str(topic);
//        for(unsigned int i=0;i<child[str].size();i++){
//            char temp[20];
//            sprintf(temp,"node[%d].zigbee",child[str][i]);
//            msg->setDestinationID(simulation.getModuleByPath(temp)->getId());
//            send(msg->dup(),"io$o");
//        }
//        EV<<"Data Message"<<msg->getTopic()<<".\n";


}





void Sensor::finish(){
     recordScalar("Connected",connected);

//    EV<<AreaID<<"###"<<parent["Area-11/client-5/Temperature"]<<"@@";
//    for(unsigned int i=0;i<child["Area-11/client-5/Temperature"].size();i++){
//        EV<<child["Area-11/client-5/Temperature"][i]<<"@@";
//    }
//
//    EV<<"\n";

//    EV<<"@@@@@@@@@@@@@"<<SubscriptionTopic<<"\n";
//
//    for(unsigned i=0;i<data_rec.size();i++){
//       EV<<"##"<<data_rec[i]<<"\n";
//    }



    //EV<<"test="<<test;
}

simtime_t Sensor::getNextTransmissionTime()
{
    simtime_t t = simTime()+time_interval;
    return t;

}




