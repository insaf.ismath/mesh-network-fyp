//
// Generated file, do not edit! Created by nedtool 4.6 from message.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "message_m.h"

USING_NAMESPACE


// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




// Template rule for outputting std::vector<T> types
template<typename T, typename A>
inline std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec)
{
    out.put('{');
    for(typename std::vector<T,A>::const_iterator it = vec.begin(); it != vec.end(); ++it)
    {
        if (it != vec.begin()) {
            out.put(','); out.put(' ');
        }
        out << *it;
    }
    out.put('}');
    
    char buf[32];
    sprintf(buf, " (size=%u)", (unsigned int)vec.size());
    out.write(buf, strlen(buf));
    return out;
}

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
inline std::ostream& operator<<(std::ostream& out,const T&) {return out;}

Register_Class(Message);

Message::Message(const char *name, int kind) : ::cPacket(name,kind)
{
    this->NodeID_var = 0;
    this->PacketID_var = 0;
    this->TopicID_var = 0;
    this->type_var = 0;
    this->topic_var = "NULL";
    this->data_var = 0;
    this->SourceID_var = 0;
    this->DestinationID_var = 0;
    this->Tx_power_var = 0;
}

Message::Message(const Message& other) : ::cPacket(other)
{
    copy(other);
}

Message::~Message()
{
}

Message& Message::operator=(const Message& other)
{
    if (this==&other) return *this;
    ::cPacket::operator=(other);
    copy(other);
    return *this;
}

void Message::copy(const Message& other)
{
    this->NodeID_var = other.NodeID_var;
    this->PacketID_var = other.PacketID_var;
    this->TopicID_var = other.TopicID_var;
    this->type_var = other.type_var;
    this->topic_var = other.topic_var;
    this->data_var = other.data_var;
    this->SourceID_var = other.SourceID_var;
    this->DestinationID_var = other.DestinationID_var;
    this->Tx_power_var = other.Tx_power_var;
}

void Message::parsimPack(cCommBuffer *b)
{
    ::cPacket::parsimPack(b);
    doPacking(b,this->NodeID_var);
    doPacking(b,this->PacketID_var);
    doPacking(b,this->TopicID_var);
    doPacking(b,this->type_var);
    doPacking(b,this->topic_var);
    doPacking(b,this->data_var);
    doPacking(b,this->SourceID_var);
    doPacking(b,this->DestinationID_var);
    doPacking(b,this->Tx_power_var);
}

void Message::parsimUnpack(cCommBuffer *b)
{
    ::cPacket::parsimUnpack(b);
    doUnpacking(b,this->NodeID_var);
    doUnpacking(b,this->PacketID_var);
    doUnpacking(b,this->TopicID_var);
    doUnpacking(b,this->type_var);
    doUnpacking(b,this->topic_var);
    doUnpacking(b,this->data_var);
    doUnpacking(b,this->SourceID_var);
    doUnpacking(b,this->DestinationID_var);
    doUnpacking(b,this->Tx_power_var);
}

int Message::getNodeID() const
{
    return NodeID_var;
}

void Message::setNodeID(int NodeID)
{
    this->NodeID_var = NodeID;
}

int Message::getPacketID() const
{
    return PacketID_var;
}

void Message::setPacketID(int PacketID)
{
    this->PacketID_var = PacketID;
}

int Message::getTopicID() const
{
    return TopicID_var;
}

void Message::setTopicID(int TopicID)
{
    this->TopicID_var = TopicID;
}

const char * Message::getType() const
{
    return type_var.c_str();
}

void Message::setType(const char * type)
{
    this->type_var = type;
}

const char * Message::getTopic() const
{
    return topic_var.c_str();
}

void Message::setTopic(const char * topic)
{
    this->topic_var = topic;
}

const char * Message::getData() const
{
    return data_var.c_str();
}

void Message::setData(const char * data)
{
    this->data_var = data;
}

int Message::getSourceID() const
{
    return SourceID_var;
}

void Message::setSourceID(int SourceID)
{
    this->SourceID_var = SourceID;
}

int Message::getDestinationID() const
{
    return DestinationID_var;
}

void Message::setDestinationID(int DestinationID)
{
    this->DestinationID_var = DestinationID;
}

int Message::getTx_power() const
{
    return Tx_power_var;
}

void Message::setTx_power(int Tx_power)
{
    this->Tx_power_var = Tx_power;
}

class MessageDescriptor : public cClassDescriptor
{
  public:
    MessageDescriptor();
    virtual ~MessageDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(MessageDescriptor);

MessageDescriptor::MessageDescriptor() : cClassDescriptor("Message", "cPacket")
{
}

MessageDescriptor::~MessageDescriptor()
{
}

bool MessageDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<Message *>(obj)!=NULL;
}

const char *MessageDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int MessageDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 9+basedesc->getFieldCount(object) : 9;
}

unsigned int MessageDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<9) ? fieldTypeFlags[field] : 0;
}

const char *MessageDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "NodeID",
        "PacketID",
        "TopicID",
        "type",
        "topic",
        "data",
        "SourceID",
        "DestinationID",
        "Tx_power",
    };
    return (field>=0 && field<9) ? fieldNames[field] : NULL;
}

int MessageDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='N' && strcmp(fieldName, "NodeID")==0) return base+0;
    if (fieldName[0]=='P' && strcmp(fieldName, "PacketID")==0) return base+1;
    if (fieldName[0]=='T' && strcmp(fieldName, "TopicID")==0) return base+2;
    if (fieldName[0]=='t' && strcmp(fieldName, "type")==0) return base+3;
    if (fieldName[0]=='t' && strcmp(fieldName, "topic")==0) return base+4;
    if (fieldName[0]=='d' && strcmp(fieldName, "data")==0) return base+5;
    if (fieldName[0]=='S' && strcmp(fieldName, "SourceID")==0) return base+6;
    if (fieldName[0]=='D' && strcmp(fieldName, "DestinationID")==0) return base+7;
    if (fieldName[0]=='T' && strcmp(fieldName, "Tx_power")==0) return base+8;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *MessageDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
        "int",
        "string",
        "string",
        "string",
        "int",
        "int",
        "int",
    };
    return (field>=0 && field<9) ? fieldTypeStrings[field] : NULL;
}

const char *MessageDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int MessageDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    Message *pp = (Message *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string MessageDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    Message *pp = (Message *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getNodeID());
        case 1: return long2string(pp->getPacketID());
        case 2: return long2string(pp->getTopicID());
        case 3: return oppstring2string(pp->getType());
        case 4: return oppstring2string(pp->getTopic());
        case 5: return oppstring2string(pp->getData());
        case 6: return long2string(pp->getSourceID());
        case 7: return long2string(pp->getDestinationID());
        case 8: return long2string(pp->getTx_power());
        default: return "";
    }
}

bool MessageDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    Message *pp = (Message *)object; (void)pp;
    switch (field) {
        case 0: pp->setNodeID(string2long(value)); return true;
        case 1: pp->setPacketID(string2long(value)); return true;
        case 2: pp->setTopicID(string2long(value)); return true;
        case 3: pp->setType((value)); return true;
        case 4: pp->setTopic((value)); return true;
        case 5: pp->setData((value)); return true;
        case 6: pp->setSourceID(string2long(value)); return true;
        case 7: pp->setDestinationID(string2long(value)); return true;
        case 8: pp->setTx_power(string2long(value)); return true;
        default: return false;
    }
}

const char *MessageDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    };
}

void *MessageDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    Message *pp = (Message *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}


