//
// Generated file, do not edit! Created by nedtool 4.6 from routing_message.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "routing_message_m.h"

USING_NAMESPACE


// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




// Template rule for outputting std::vector<T> types
template<typename T, typename A>
inline std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec)
{
    out.put('{');
    for(typename std::vector<T,A>::const_iterator it = vec.begin(); it != vec.end(); ++it)
    {
        if (it != vec.begin()) {
            out.put(','); out.put(' ');
        }
        out << *it;
    }
    out.put('}');
    
    char buf[32];
    sprintf(buf, " (size=%u)", (unsigned int)vec.size());
    out.write(buf, strlen(buf));
    return out;
}

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
inline std::ostream& operator<<(std::ostream& out,const T&) {return out;}

Register_Class(Routing_message);

Routing_message::Routing_message(const char *name, int kind) : ::cPacket(name,kind)
{
    this->rank_var = 0;
    this->sourceID_var = 0;
    this->concentratorID_var = 0;
    this->totalHopEnergy_var = 0;
    this->noHopsToConcentrator_var = 0;
}

Routing_message::Routing_message(const Routing_message& other) : ::cPacket(other)
{
    copy(other);
}

Routing_message::~Routing_message()
{
}

Routing_message& Routing_message::operator=(const Routing_message& other)
{
    if (this==&other) return *this;
    ::cPacket::operator=(other);
    copy(other);
    return *this;
}

void Routing_message::copy(const Routing_message& other)
{
    this->rank_var = other.rank_var;
    this->sourceID_var = other.sourceID_var;
    this->concentratorID_var = other.concentratorID_var;
    this->totalHopEnergy_var = other.totalHopEnergy_var;
    this->noHopsToConcentrator_var = other.noHopsToConcentrator_var;
}

void Routing_message::parsimPack(cCommBuffer *b)
{
    ::cPacket::parsimPack(b);
    doPacking(b,this->rank_var);
    doPacking(b,this->sourceID_var);
    doPacking(b,this->concentratorID_var);
    doPacking(b,this->totalHopEnergy_var);
    doPacking(b,this->noHopsToConcentrator_var);
}

void Routing_message::parsimUnpack(cCommBuffer *b)
{
    ::cPacket::parsimUnpack(b);
    doUnpacking(b,this->rank_var);
    doUnpacking(b,this->sourceID_var);
    doUnpacking(b,this->concentratorID_var);
    doUnpacking(b,this->totalHopEnergy_var);
    doUnpacking(b,this->noHopsToConcentrator_var);
}

double Routing_message::getRank() const
{
    return rank_var;
}

void Routing_message::setRank(double rank)
{
    this->rank_var = rank;
}

int Routing_message::getSourceID() const
{
    return sourceID_var;
}

void Routing_message::setSourceID(int sourceID)
{
    this->sourceID_var = sourceID;
}

int Routing_message::getConcentratorID() const
{
    return concentratorID_var;
}

void Routing_message::setConcentratorID(int concentratorID)
{
    this->concentratorID_var = concentratorID;
}

double Routing_message::getTotalHopEnergy() const
{
    return totalHopEnergy_var;
}

void Routing_message::setTotalHopEnergy(double totalHopEnergy)
{
    this->totalHopEnergy_var = totalHopEnergy;
}

int Routing_message::getNoHopsToConcentrator() const
{
    return noHopsToConcentrator_var;
}

void Routing_message::setNoHopsToConcentrator(int noHopsToConcentrator)
{
    this->noHopsToConcentrator_var = noHopsToConcentrator;
}

class Routing_messageDescriptor : public cClassDescriptor
{
  public:
    Routing_messageDescriptor();
    virtual ~Routing_messageDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(Routing_messageDescriptor);

Routing_messageDescriptor::Routing_messageDescriptor() : cClassDescriptor("Routing_message", "cPacket")
{
}

Routing_messageDescriptor::~Routing_messageDescriptor()
{
}

bool Routing_messageDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<Routing_message *>(obj)!=NULL;
}

const char *Routing_messageDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int Routing_messageDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 5+basedesc->getFieldCount(object) : 5;
}

unsigned int Routing_messageDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<5) ? fieldTypeFlags[field] : 0;
}

const char *Routing_messageDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "rank",
        "sourceID",
        "concentratorID",
        "totalHopEnergy",
        "noHopsToConcentrator",
    };
    return (field>=0 && field<5) ? fieldNames[field] : NULL;
}

int Routing_messageDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='r' && strcmp(fieldName, "rank")==0) return base+0;
    if (fieldName[0]=='s' && strcmp(fieldName, "sourceID")==0) return base+1;
    if (fieldName[0]=='c' && strcmp(fieldName, "concentratorID")==0) return base+2;
    if (fieldName[0]=='t' && strcmp(fieldName, "totalHopEnergy")==0) return base+3;
    if (fieldName[0]=='n' && strcmp(fieldName, "noHopsToConcentrator")==0) return base+4;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *Routing_messageDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "double",
        "int",
        "int",
        "double",
        "int",
    };
    return (field>=0 && field<5) ? fieldTypeStrings[field] : NULL;
}

const char *Routing_messageDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int Routing_messageDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    Routing_message *pp = (Routing_message *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string Routing_messageDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    Routing_message *pp = (Routing_message *)object; (void)pp;
    switch (field) {
        case 0: return double2string(pp->getRank());
        case 1: return long2string(pp->getSourceID());
        case 2: return long2string(pp->getConcentratorID());
        case 3: return double2string(pp->getTotalHopEnergy());
        case 4: return long2string(pp->getNoHopsToConcentrator());
        default: return "";
    }
}

bool Routing_messageDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    Routing_message *pp = (Routing_message *)object; (void)pp;
    switch (field) {
        case 0: pp->setRank(string2double(value)); return true;
        case 1: pp->setSourceID(string2long(value)); return true;
        case 2: pp->setConcentratorID(string2long(value)); return true;
        case 3: pp->setTotalHopEnergy(string2double(value)); return true;
        case 4: pp->setNoHopsToConcentrator(string2long(value)); return true;
        default: return false;
    }
}

const char *Routing_messageDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    };
}

void *Routing_messageDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    Routing_message *pp = (Routing_message *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}


