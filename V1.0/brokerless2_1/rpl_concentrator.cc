/*
 * NetworkLayerConcentator.cc
 *
 *  Created on: Aug 23, 2018
 *      Author: Insaf Ismath
 */
#include <omnetpp.h>
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <map>
#include "message_m.h"
#include "tree_m.h"

#include "data_message_m.h"
#include "routing_message_m.h"

using namespace std;

class NetworkLayerConcentator : public cSimpleModule
{
    private:
        int SourceID;
        int DestinationID;
        int NodeID;
        int PacketID;

        double iaTime=exponential(2);

        Tree *tree_start_trigger;
        Message *triggermsg;
        Message *routing_trigger;





        simtime_t response_time;
        simtime_t initialize_time;
        simtime_t connectable_time=100;

    public:


    protected:
        virtual void initialize();
        virtual void handleMessage(cMessage *msg);
        virtual void sendRoutingMessage();
        virtual void finish();

        simtime_t getNextTransmissionTime();
};

Define_Module(NetworkLayerConcentator);

void NetworkLayerConcentator::initialize()
{
    // TODO - Generated method body
    NodeID=this->getParentModule()->getIndex();
    SourceID =this->getParentModule()->getSubmodule("zigbee")->getId();

    routing_trigger=new Message("routing_trigger");

    scheduleAt(simTime()+2,routing_trigger);

}


void NetworkLayerConcentator::handleMessage(cMessage *msg)
{
    if(strcmp(msg->getClassName(),"Message")==0)
    {
        Message *rec_msg = check_and_cast<Message *>(msg);

        if(rec_msg==routing_trigger){
            sendRoutingMessage();
//            scheduleAt(simTime() + 30, routing_trigger);
        }
    }
}

void NetworkLayerConcentator::sendRoutingMessage(){

    Routing_message *out_msg = new Routing_message("Initial_Routing_message");

    double rank = 0;

    out_msg->setRank(rank);
    out_msg->setSourceID(NodeID);
    out_msg->setConcentratorID(NodeID);
    out_msg->setTotalHopEnergy(0);
    out_msg->setNoHopsToConcentrator(1);

    send(out_msg,"io$o");


}



void NetworkLayerConcentator::finish(){

}

simtime_t NetworkLayerConcentator::getNextTransmissionTime()
{
    simtime_t t = simTime()+iaTime;
    return t;

}




