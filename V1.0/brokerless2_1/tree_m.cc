//
// Generated file, do not edit! Created by nedtool 4.6 from tree.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "tree_m.h"

USING_NAMESPACE


// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




// Template rule for outputting std::vector<T> types
template<typename T, typename A>
inline std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec)
{
    out.put('{');
    for(typename std::vector<T,A>::const_iterator it = vec.begin(); it != vec.end(); ++it)
    {
        if (it != vec.begin()) {
            out.put(','); out.put(' ');
        }
        out << *it;
    }
    out.put('}');
    
    char buf[32];
    sprintf(buf, " (size=%u)", (unsigned int)vec.size());
    out.write(buf, strlen(buf));
    return out;
}

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
inline std::ostream& operator<<(std::ostream& out,const T&) {return out;}

Register_Class(Tree);

Tree::Tree(const char *name, int kind) : ::cPacket(name,kind)
{
    this->topic_var = 0;
    this->broadcast_var = false;
    this->type_var = 0;
    this->SourceID_var = 0;
    this->DestinationID_var = 0;
    this->Cost_var = 0;
    this->Tx_power_var = 0;
}

Tree::Tree(const Tree& other) : ::cPacket(other)
{
    copy(other);
}

Tree::~Tree()
{
}

Tree& Tree::operator=(const Tree& other)
{
    if (this==&other) return *this;
    ::cPacket::operator=(other);
    copy(other);
    return *this;
}

void Tree::copy(const Tree& other)
{
    this->topic_var = other.topic_var;
    this->broadcast_var = other.broadcast_var;
    this->type_var = other.type_var;
    this->SourceID_var = other.SourceID_var;
    this->DestinationID_var = other.DestinationID_var;
    this->Cost_var = other.Cost_var;
    this->Tx_power_var = other.Tx_power_var;
}

void Tree::parsimPack(cCommBuffer *b)
{
    ::cPacket::parsimPack(b);
    doPacking(b,this->topic_var);
    doPacking(b,this->broadcast_var);
    doPacking(b,this->type_var);
    doPacking(b,this->SourceID_var);
    doPacking(b,this->DestinationID_var);
    doPacking(b,this->Cost_var);
    doPacking(b,this->Tx_power_var);
}

void Tree::parsimUnpack(cCommBuffer *b)
{
    ::cPacket::parsimUnpack(b);
    doUnpacking(b,this->topic_var);
    doUnpacking(b,this->broadcast_var);
    doUnpacking(b,this->type_var);
    doUnpacking(b,this->SourceID_var);
    doUnpacking(b,this->DestinationID_var);
    doUnpacking(b,this->Cost_var);
    doUnpacking(b,this->Tx_power_var);
}

const char * Tree::getTopic() const
{
    return topic_var.c_str();
}

void Tree::setTopic(const char * topic)
{
    this->topic_var = topic;
}

bool Tree::getBroadcast() const
{
    return broadcast_var;
}

void Tree::setBroadcast(bool broadcast)
{
    this->broadcast_var = broadcast;
}

const char * Tree::getType() const
{
    return type_var.c_str();
}

void Tree::setType(const char * type)
{
    this->type_var = type;
}

int Tree::getSourceID() const
{
    return SourceID_var;
}

void Tree::setSourceID(int SourceID)
{
    this->SourceID_var = SourceID;
}

int Tree::getDestinationID() const
{
    return DestinationID_var;
}

void Tree::setDestinationID(int DestinationID)
{
    this->DestinationID_var = DestinationID;
}

int Tree::getCost() const
{
    return Cost_var;
}

void Tree::setCost(int Cost)
{
    this->Cost_var = Cost;
}

int Tree::getTx_power() const
{
    return Tx_power_var;
}

void Tree::setTx_power(int Tx_power)
{
    this->Tx_power_var = Tx_power;
}

class TreeDescriptor : public cClassDescriptor
{
  public:
    TreeDescriptor();
    virtual ~TreeDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(TreeDescriptor);

TreeDescriptor::TreeDescriptor() : cClassDescriptor("Tree", "cPacket")
{
}

TreeDescriptor::~TreeDescriptor()
{
}

bool TreeDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<Tree *>(obj)!=NULL;
}

const char *TreeDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int TreeDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 7+basedesc->getFieldCount(object) : 7;
}

unsigned int TreeDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<7) ? fieldTypeFlags[field] : 0;
}

const char *TreeDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "topic",
        "broadcast",
        "type",
        "SourceID",
        "DestinationID",
        "Cost",
        "Tx_power",
    };
    return (field>=0 && field<7) ? fieldNames[field] : NULL;
}

int TreeDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='t' && strcmp(fieldName, "topic")==0) return base+0;
    if (fieldName[0]=='b' && strcmp(fieldName, "broadcast")==0) return base+1;
    if (fieldName[0]=='t' && strcmp(fieldName, "type")==0) return base+2;
    if (fieldName[0]=='S' && strcmp(fieldName, "SourceID")==0) return base+3;
    if (fieldName[0]=='D' && strcmp(fieldName, "DestinationID")==0) return base+4;
    if (fieldName[0]=='C' && strcmp(fieldName, "Cost")==0) return base+5;
    if (fieldName[0]=='T' && strcmp(fieldName, "Tx_power")==0) return base+6;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *TreeDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "string",
        "bool",
        "string",
        "int",
        "int",
        "int",
        "int",
    };
    return (field>=0 && field<7) ? fieldTypeStrings[field] : NULL;
}

const char *TreeDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int TreeDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    Tree *pp = (Tree *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string TreeDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    Tree *pp = (Tree *)object; (void)pp;
    switch (field) {
        case 0: return oppstring2string(pp->getTopic());
        case 1: return bool2string(pp->getBroadcast());
        case 2: return oppstring2string(pp->getType());
        case 3: return long2string(pp->getSourceID());
        case 4: return long2string(pp->getDestinationID());
        case 5: return long2string(pp->getCost());
        case 6: return long2string(pp->getTx_power());
        default: return "";
    }
}

bool TreeDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    Tree *pp = (Tree *)object; (void)pp;
    switch (field) {
        case 0: pp->setTopic((value)); return true;
        case 1: pp->setBroadcast(string2bool(value)); return true;
        case 2: pp->setType((value)); return true;
        case 3: pp->setSourceID(string2long(value)); return true;
        case 4: pp->setDestinationID(string2long(value)); return true;
        case 5: pp->setCost(string2long(value)); return true;
        case 6: pp->setTx_power(string2long(value)); return true;
        default: return false;
    }
}

const char *TreeDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    };
}

void *TreeDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    Tree *pp = (Tree *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}


