/*
 * sensor.cc
 *
 *  Created on: Dec 28, 2017
 *      Author: Arun Kumar
 */
#include <omnetpp.h>
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <map>
#include "message_m.h"
#include "tree_m.h"

using namespace std;

class Sensor : public cSimpleModule
{
    private:
        int SourceID;
        int DestinationID;
        int NodeID;
        int PacketID;
        bool publisher=false;
        bool subscriber=false;
        double iaTime=exponential(2);

        int x_cor,y_cor;
        int AreaID;
        int test;

        int tree_starter_count=0;
        char SubscriptionTopic[50];
        simtime_t timeout=2.0;
        string my_topic="*************************";

        vector<string> data_rec;
        vector<string> sub_topic;
        vector<string> key;
        map<string,int> cost;
        map<string,int> parent;
        map<string,vector<int>> child;



        cOutVector end_delay;
        int connected=0;


        Tree *tree_start_trigger;
        Message *triggermsg;
        Message *selftest;


        simtime_t response_time;
        simtime_t initialize_time;
        simtime_t connectable_time=100;

    public:


    protected:
        virtual void initialize();
        virtual void handleMessage(cMessage *msg);
        virtual void publish();
        virtual void forwarding(Message *msg);
        virtual void treestarter();
        virtual void broadcast(string,int);
        virtual void becomechild(string topic,int oldparent,int newparent);
        virtual void finish();

        simtime_t getNextTransmissionTime();
};

Define_Module(Sensor);

void Sensor::initialize()
{
    // TODO - Generated method body
    NodeID=this->getParentModule()->getIndex();
    SourceID =this->getParentModule()->getSubmodule("zigbee")->getId();
    x_cor=atoi(this->getParentModule()->getDisplayString().getTagArg("p",0))-50;
    y_cor=atoi(this->getParentModule()->getDisplayString().getTagArg("p",1))-50;
    int x_temp,y_temp;
    if(x_cor<=300) x_temp=1;
    else if(x_cor<=600) x_temp=2;
    else x_temp=3;

    if(y_cor<=300) y_temp=1;
    else if(y_cor<=600) y_temp=2;
    else y_temp=3;


    AreaID=10*y_temp+x_temp;
//    char temp[10];
//    sprintf(temp,"%d",AreaID);
//    this->getParentModule()->getDisplayString().setTagArg("t",0,temp);

    end_delay.setName("Delay_time");


    //SUBSCRIPTION//////////////////////////////////////////////
    sprintf(SubscriptionTopic,"****************");
    double sub_percent=this->getAncestorPar("subscriber_percentage");
    if(uniform(0,1)<sub_percent){
        subscriber=true;
        this->getParentModule()->getDisplayString().setTagArg("i",1,"#009900");
        //this->getParentModule()->getDisplayString().setTagArg("t",0,"Sub");
        int i=intuniform(1,12);
        switch(i){
        case(1):sprintf(SubscriptionTopic,"Area-11/*/*");this->getParentModule()->getDisplayString().setTagArg("t",0,"A-11");break;
        case(2):sprintf(SubscriptionTopic,"Area-12/*/*");this->getParentModule()->getDisplayString().setTagArg("t",0,"A-12");break;
        case(3):sprintf(SubscriptionTopic,"Area-13/*/*");this->getParentModule()->getDisplayString().setTagArg("t",0,"A-13");break;
        case(4):sprintf(SubscriptionTopic,"Area-21/*/*");this->getParentModule()->getDisplayString().setTagArg("t",0,"A-21");break;
        case(5):sprintf(SubscriptionTopic,"Area-22/*/*");this->getParentModule()->getDisplayString().setTagArg("t",0,"A-22");break;
        case(6):sprintf(SubscriptionTopic,"Area-23/*/*");this->getParentModule()->getDisplayString().setTagArg("t",0,"A-23");break;
        case(7):sprintf(SubscriptionTopic,"Area-31/*/*");this->getParentModule()->getDisplayString().setTagArg("t",0,"A-31");break;
        case(8):sprintf(SubscriptionTopic,"Area-32/*/*");this->getParentModule()->getDisplayString().setTagArg("t",0,"A-32");break;
        case(9):sprintf(SubscriptionTopic,"Area-33/*/*");this->getParentModule()->getDisplayString().setTagArg("t",0,"A-33");break;
        case(10):sprintf(SubscriptionTopic,"*/*/Temperature");this->getParentModule()->getDisplayString().setTagArg("t",0,"T");break;
        case(11):sprintf(SubscriptionTopic,"*/*/Humidity");this->getParentModule()->getDisplayString().setTagArg("t",0,"H");break;
        case(12):sprintf(SubscriptionTopic,"*/*/Light_Intensity");this->getParentModule()->getDisplayString().setTagArg("t",0,"L_I");break;


        }



    }
    //Checking for PUBLISHER/////////////////////////////////////////////
    double pub_percent=this->getAncestorPar("publisher_percentage");
    if(uniform(0,1)<pub_percent){
        publisher=true;
        char temp[80];
        sprintf(temp,"Area-%d/client-%d/****",AreaID,NodeID);
        string str(temp);
        my_topic=str;
        if(subscriber){
            this->getParentModule()->getDisplayString().setTagArg("i",1,"#005ce6");
        }
        else{
            this->getParentModule()->getDisplayString().setTagArg("i",1,"#000099");
        }
        tree_start_trigger = new Tree("TreeStarter");
        //if(AreaID==11) scheduleAt(getNextTransmissionTime(),tree_start_trigger);
        scheduleAt(getNextTransmissionTime(),tree_start_trigger);

    }
    selftest=new Message("SelfTest");
    if (subscriber) scheduleAt(simTime()+5,selftest);

//    if(!strcmp(SubscriptionTopic,"Area-11/*/*")||!strcmp(SubscriptionTopic,"*/*/Humidity")){
//        this->getParentModule()->getDisplayString().setTagArg("i",1,"black");
//
//    }

}


void Sensor::handleMessage(cMessage *msg)
{
    if(strcmp(msg->getClassName(),"Message")==0)
    {
        Message *rec_msg = check_and_cast<Message *>(msg);
        if(rec_msg==triggermsg){
            publish();
            scheduleAt(simTime()+1.67,triggermsg);

        }

        else if(rec_msg==selftest){
            bool temp;
            for(unsigned int i=0;i<sub_topic.size();i++){
                if(data_rec.size()==0 || find(data_rec.begin(), data_rec.end(),sub_topic[i]) == data_rec.end()){
                    becomechild(sub_topic[i],-1,parent[sub_topic[i]]);
                    temp=true;
                }
            }
            if(temp)    scheduleAt(simTime()+5,selftest);

        }



        else{
            if(strcmp(rec_msg->getType(),"DATA")==0){
                if(find(data_rec.begin(), data_rec.end(),rec_msg->getTopic()) == data_rec.end()){
                    data_rec.push_back(rec_msg->getTopic());
                }
                //forwarding(rec_msg);
                if(simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex()!=parent[rec_msg->getTopic()]){
                    becomechild(rec_msg->getTopic(),simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex(),-1);
                }
                else {
                    forwarding(rec_msg);
                }

                string rec_topic=rec_msg->getTopic();
                int second=rec_topic.find("/",9);

                string topic_check=rec_topic.substr(0,8)+"*"+rec_topic.substr(second);
                string SubTop(SubscriptionTopic);
                if((SubTop.substr(0,7)==topic_check.substr(0,7) || SubTop.substr(4)==topic_check.substr(10)) && subscriber){
                    end_delay.record(simTime()-rec_msg->getTimestamp());
                    connected=1;
                }

            }
        }
    }

    else if(strcmp(msg->getClassName(),"Tree")==0)
    {
        Tree *rec_msg = check_and_cast<Tree *>(msg);
        if(rec_msg==tree_start_trigger){
            treestarter();



        }


        else{

            string rec_topic=rec_msg->getTopic();
            int second=rec_topic.find("/",9);

            string topic_check=rec_topic.substr(0,8)+"*"+rec_topic.substr(second);
            string SubTop(SubscriptionTopic);
            if(strcmp(rec_msg->getType(),"TreeMsg")==0 && subscriber){// && rec_topic.substr(0,second)!=my_topic.substr(0,second)){

                EV<<topic_check<<"\n";
                EV<<SubscriptionTopic<<"\n";
                EV<<SubTop.substr(0,7)<<"@@"<<SubTop.substr(4)<<"\n";
                EV<<topic_check.substr(0,7)<<"@@"<<topic_check.substr(10)<<"\n";
                //////////////////////////////////////////////////////////////////////////////////////////////
                int cost_increment;
                if(SubTop.substr(0,7)==topic_check.substr(0,7)){
                    cost_increment=1;
                }
                else if(SubTop.substr(4)==topic_check.substr(10)){
                    cost_increment=3;
                }
                else{//not interested
                    if(!publisher) {
                        cost_increment=50;
                    }
                    else {
                        cost_increment=200;
                    }
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////

                if(find(key.begin(), key.end(),rec_topic) == key.end()){
                    key.push_back(rec_topic);
                    cost[rec_topic]=rec_msg->getCost();
                    if(cost_increment<=3){
                        //becomechild
                        becomechild(rec_topic,-1,simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex());
                    }
                    //update parent table
                    parent[rec_topic]=simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex();

                    //broadcast
                    broadcast(rec_topic,rec_msg->getCost()+cost_increment+child[rec_topic].size());

//                    char temp[20];
//                    sprintf(temp,"%d",cost[rec_topic]+cost_increment+child[rec_topic].size());
//                    this->getParentModule()->getDisplayString().setTagArg("t",0,temp);


                }
                else{
                    if(rec_msg->getCost() < cost[rec_topic]){
                        //EV<<"running\n";
                        //short route
                        cost[rec_topic]=rec_msg->getCost();//+cost_increment+child[rec_topic].size();
//                        char temp[20];
//                        sprintf(temp,"%d",cost[rec_topic]+cost_increment+child[rec_topic].size());
//                        this->getParentModule()->getDisplayString().setTagArg("t",0,temp);

                        //becomechild
                        if(cost_increment<=3 && (parent[rec_topic]!=simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex())){
                            becomechild(rec_topic,parent[rec_topic],simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex());
                        }

                        //update parent table
                        parent[rec_topic]=simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex();

                        //broadcast
                        broadcast(rec_topic,rec_msg->getCost()+cost_increment+child[rec_topic].size());


                    }
                }
            }

            else if(strcmp(rec_msg->getType(),"ChildMsg")==0){
                //if(NodeID==96 && simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex()==26)   test=1;
                if(!(SubTop.substr(0,7)==topic_check.substr(0,7) || SubTop.substr(4)==topic_check.substr(10)) && subscriber && child[rec_topic].size()==0){
                    becomechild(rec_topic,-1,parent[rec_topic]);
                }

                //update child table (Add to child)

                if(find(child[rec_topic].begin(), child[rec_topic].end(),simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex()) == child[rec_topic].end()){
                    child[rec_topic].push_back(simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex());
                }

            }

            else if(strcmp(rec_msg->getType(),"NoChildMsg")==0){
                //update child table (remove from child)
                int val=simulation.getModule(rec_msg->getSourceID())->getParentModule()->getIndex();
                child[rec_topic].erase(std::remove(child[rec_topic].begin(), child[rec_topic].end(), val), child[rec_topic].end());
                if(!(SubTop.substr(0,7)==topic_check.substr(0,7) || SubTop.substr(4)==topic_check.substr(10)) && subscriber && child[rec_topic].size()==0){ //interested and subscribers
                    becomechild(rec_topic,parent[rec_topic],-1);

                }

            }
        }
    }
}

void Sensor::publish(){
    Message *msg = new Message("DATA MESSAGE");
        PacketID=intuniform(0,1000);
        int topic_sel=intuniform(0,2);
        char data_type[20];
        switch(topic_sel){
        case(0):strcpy(data_type,"Temperature");
        break;
        case(1):strcpy(data_type,"Humidity");
        break;
        case(2):strcpy(data_type,"Light_Intensity");
        break;
        }
        char topic[80];
        sprintf(topic,"Area-%d/client-%d/",AreaID,NodeID);
        strcat(topic,data_type);

        msg->setType("DATA");
        msg->setPacketID(PacketID);
        msg->setTopic(topic);
        msg->setData("#######");
        msg->setSourceID(SourceID);
        msg->setTimestamp();

        string str(topic);
        for(unsigned int i=0;i<child[str].size();i++){
            char temp[20];
            sprintf(temp,"node[%d].zigbee",child[str][i]);
            msg->setDestinationID(simulation.getModuleByPath(temp)->getId());
            send(msg->dup(),"io$o");
        }
        EV<<"Data Message"<<msg->getTopic()<<".\n";


}

void Sensor::forwarding(Message *msg){
    string str(msg->getTopic());
    msg->setSourceID(SourceID);

    for(unsigned int i=0;i<child[str].size();i++){
        char temp[20];
        sprintf(temp,"node[%d].zigbee",child[str][i]);
        msg->setDestinationID(simulation.getModuleByPath(temp)->getId());
        send(msg->dup(),"io$o");
    }


}

void Sensor::treestarter(){

    if(publisher){
        char temp[80];

        switch(tree_starter_count){
        case(0):sprintf(temp,"Area-%d/client-%d/Temperature",AreaID,NodeID);break;
        case(1):sprintf(temp,"Area-%d/client-%d/Humidity",AreaID,NodeID);break;
        case(2):sprintf(temp,"Area-%d/client-%d/Light_Intensity",AreaID,NodeID);break;
        }
        //sprintf(temp,"Area-%d/client-%d/Humidity",AreaID,NodeID);   //test

        broadcast(temp,0);
        string str(temp);
        key.push_back(str);
        cost[str]=0;

        tree_starter_count++;
        //EV<<temp<<"\n";

        if(tree_starter_count<3) scheduleAt(getNextTransmissionTime(),tree_start_trigger);
        else{
            triggermsg = new Message("Publish");
            scheduleAt(getNextTransmissionTime(),triggermsg);
        }


        ///////////////////////need to think for 3 topics///////////////
    }

}

void Sensor::broadcast(string topic,int cost){
        char mytopic[80];
        sprintf(mytopic,"%s",topic.c_str());
        Tree *broadcast_msg= new Tree("BroadcastMsg");
        broadcast_msg->setType("TreeMsg");
        broadcast_msg->setBroadcast(true);
        broadcast_msg->setSourceID(SourceID);
        broadcast_msg->setTopic(mytopic);
        broadcast_msg->setCost(cost);
        send(broadcast_msg,"io$o");

}



void Sensor::becomechild(string topic,int oldparent,int newparent){
    int second=topic.find("/",9);
    if(topic.substr(0,second)!=my_topic.substr(0,second)){
    //send no child message

    if(oldparent!=-1){
        Tree *nochild_msg= new Tree("NoChildMsg");
        nochild_msg->setType("NoChildMsg");
        nochild_msg->setSourceID(SourceID);
        char temp[20];
        sprintf(temp,"node[%d].zigbee",oldparent);
        nochild_msg->setDestinationID(simulation.getModuleByPath(temp)->getId());
        nochild_msg->setTopic(topic.c_str());
        nochild_msg->setBroadcast(false);
        send(nochild_msg,"io$o");
        sub_topic.erase(std::remove(sub_topic.begin(), sub_topic.end(), topic), sub_topic.end());
    }
    //send child to rec_msg->getNodeID()
    if(newparent!=-1){
        Tree *child_msg= new Tree("ChildMsg");
        child_msg->setType("ChildMsg");
        child_msg->setSourceID(SourceID);
        char temp[20];
        sprintf(temp,"node[%d].zigbee",newparent);
        child_msg->setDestinationID(simulation.getModuleByPath(temp)->getId());
        child_msg->setTopic(topic.c_str());
        child_msg->setBroadcast(false);
        send(child_msg,"io$o");
        if(find(sub_topic.begin(), sub_topic.end(),topic) == sub_topic.end()){
            sub_topic.push_back(topic);
        }

    }

    }

}

void Sensor::finish(){
    if(subscriber) recordScalar("Connected",connected);

//    EV<<AreaID<<"###"<<parent["Area-11/client-5/Temperature"]<<"@@";
//    for(unsigned int i=0;i<child["Area-11/client-5/Temperature"].size();i++){
//        EV<<child["Area-11/client-5/Temperature"][i]<<"@@";
//    }
//
//    EV<<"\n";

//    EV<<"@@@@@@@@@@@@@"<<SubscriptionTopic<<"\n";
//
//    for(unsigned i=0;i<data_rec.size();i++){
//       EV<<"##"<<data_rec[i]<<"\n";
//    }



    //EV<<"test="<<test;
}

simtime_t Sensor::getNextTransmissionTime()
{
    simtime_t t = simTime()+iaTime;
    return t;

}




