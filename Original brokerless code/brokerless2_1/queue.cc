/*
 * queue.cc
 *
 *  Created on: Jan 16, 2018
 *      Author: Arun Kumar
 */

#include <omnetpp.h>
#include <cqueue.h>
#include <string.h>
#include "message_m.h"
#include "tree_m.h"


class Queue : public cSimpleModule
{
    private:
        cQueue queue;
        int queue_length;
        int neighbour_sensorID;
        int neighbour_zigbeeID;
        Message *pop;
        bool pop_scheduled;

        long rec_Packet;
        long drop_Packet;



    protected:
        virtual void initialize();
        virtual void handleMessage(cMessage *msg);
        virtual void finish();
};
Define_Module(Queue);

void Queue::initialize()
{
    // TODO - Generated method body

    queue_length=par("queue_length");
    neighbour_sensorID = gate("to_sensor$o")->getNextGate()->getOwnerModule()->getId();
    neighbour_zigbeeID = gate("to_zigbee$o")->getNextGate()->getOwnerModule()->getId();

    pop=new Message("POP");



    rec_Packet=0;
    WATCH(rec_Packet);
    drop_Packet=0;
    WATCH(drop_Packet);

}

void Queue::handleMessage(cMessage *msg)
{
    // TODO - Generated method body
    if(strcmp(msg->getClassName(),"Tree")==0){
        Tree *rec_msg = check_and_cast<Tree *>(msg);
        int senderID=rec_msg->getSenderModule()->getId();
        if(senderID==neighbour_sensorID){
            send(rec_msg,"to_zigbee$o");
        }

        else if(senderID==neighbour_zigbeeID){
            send(rec_msg,"to_sensor$o");
            rec_Packet++;
//            if(queue.length()<queue_length){
//                queue.insert(rec_msg);
//            }
//            else{
//                simulation.getModule(senderID)->getParentModule()->bubble("packet dropped");
//                drop_Packet++;
//            }
        }

    }

    else if(strcmp(msg->getClassName(),"Message")==0){
        Message *rec_msg = check_and_cast<Message *>(msg);
        int senderID=rec_msg->getSenderModule()->getId();
        if(rec_msg==pop){
            if(!queue.empty()){
            cMessage* queue_msg = (Message *)queue.pop();
            send(queue_msg,"to_sensor$o");
            pop_scheduled=false;
            }

        }

        else if(senderID==neighbour_sensorID){
            send(rec_msg,"to_zigbee$o");
        }

        else if(senderID==neighbour_zigbeeID){
            rec_Packet++;
            if(queue.length()<queue_length){
                queue.insert(rec_msg);
            }
            else{
                simulation.getModule(senderID)->getParentModule()->bubble("packet dropped");
                drop_Packet++;

            }

        }
    }


    if(!queue.empty() && !pop_scheduled) {
        pop_scheduled=true;
        scheduleAt(simTime()+0.025,pop);
    }

//    if(queue.length()==queue_length){
//        this->getParentModule()->getDisplayString().setTagArg("i",1,"#ff0000");
//    }
//    else{
//        this->getParentModule()->getDisplayString().setTagArg("i",1,"#ffffff");
//
//    }


}

void Queue::finish(){
    recordScalar("#received",rec_Packet);
    recordScalar("#dropped",drop_Packet);
    EV<<rec_Packet<<"@@@"<<drop_Packet;
}











