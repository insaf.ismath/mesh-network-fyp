/*
 * zigbee.cc
 *
 *  Created on: Dec 26, 2017
 *      Author: Arun Kumar
 */
#include <omnetpp.h>
#include "message_m.h"
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "tree_m.h"


class Zigbee : public cSimpleModule
{
  private:
        int neighbourID ;
        double pathLossExponent;
        double PLd0;
        double d0;
        double sigma;
        double noisepower;
        double SNRmin;

        int x_cor;
        int y_cor;
        int x_size;
        int y_size;

        int com_loss=0;


        //XBEE Pro Specification
        double Tx_power;
        double datarate;
        double antenna_gain;
        double rec_sensitivity;

        cOutVector BatteryUsage;
        double Battery;
        int pktsize=(10+32)*8;
        double tx_rx_time=pktsize/(250000*3600.0); //250kbps
        int i_tx;

  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void finish();
};

Define_Module(Zigbee);

void Zigbee::initialize()
{
    neighbourID = gate("node_io$o")->getNextGate()->getOwnerModule()->getId();
    //Environmental parameters////////////////////////////////////////////////////////////////////////////
    pathLossExponent=par("pathLossExponent");
    PLd0=par("PLd0");
    d0=par("d0");
    sigma=par("sigma");
    noisepower=par("noisepower");
    SNRmin=par("SNRmin");

    BatteryUsage.setName("Battery");

    //XBEE Pro Specification/////////////////////////////////////////////////////////////////////////////
    Tx_power=par("Tx_power");
    datarate=par("datarate");
    antenna_gain=par("antenna_gain");
    rec_sensitivity=par("rec_sensitivity");

    x_size=this->getAncestorPar("x_size");
    y_size=this->getAncestorPar("y_size");

    ///////////Change Position of Node here////////////////////////////////////////////////////////////
    int d=12;
    x_cor=50+(x_size/d)*intuniform(0,d);
    y_cor=50+(y_size/d)*intuniform(0,d);

    int num_created=this->getParentModule()->getIndex();
    int i=0;
    //EV<<"num_created="<<num_created<<"\n";
    while(i<num_created){
        char node_name[20];
        sprintf(node_name,"node[%d]",i);
        cModule *node=simulation.getModuleByPath(node_name);
        int x_test=atoi(node->getDisplayString().getTagArg("p",0));
        int y_test=atoi(node->getDisplayString().getTagArg("p",1));
        if((x_cor==x_test) && (y_cor==y_test)){
            x_cor=50+(x_size/d)*intuniform(0,d);
            y_cor=50+(y_size/d)*intuniform(0,d);
            i=-1;
        }
        i++;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////

    char x[20];
    sprintf(x,"%d",x_cor);

    char y[20];
    sprintf(y,"%d",y_cor);

    getParentModule()->getDisplayString().setTagArg("p",0,x);
    getParentModule()->getDisplayString().setTagArg("p",1,y);

}


void Zigbee::handleMessage(cMessage *msg)
{
    if(strcmp(msg->getClassName(),"Message")==0)
    {
        Message *rec_msg = check_and_cast<Message *>(msg);
        int senderID=rec_msg->getSenderModule()->getId();

           if(senderID==neighbourID){

               cModule *receiver=simulation.getModule(rec_msg->getDestinationID());

               int x_rec=atoi(receiver->getParentModule()->getDisplayString().getTagArg("p",0));
               int y_rec=atoi(receiver->getParentModule()->getDisplayString().getTagArg("p",1));
               int distance=sqrt(pow(x_rec-x_cor,2)+pow(y_rec-y_cor,2));

               int i_tx;
               if (distance<95.35){
                   Tx_power=10;
                   i_tx=137;
               }
               else if(distance<108.76){
                   Tx_power=12;
                   i_tx=155;
               }
               else if(distance<124.05){
                   Tx_power=14;
                   i_tx=170;
               }
               else if(distance<141.50){
                   Tx_power=16;
                   i_tx=188;
               }
               else{
                   Tx_power=18;
                   i_tx=215;
               }
               Battery+=i_tx*tx_rx_time;


               rec_msg->setTx_power(Tx_power);
               getParentModule()->getDisplayString().setTagArg("r",0,"200");
               sendDirect(rec_msg,receiver,"wireless");
               if(!strcmp(rec_msg->getTopic(),"NULL")) this->getParentModule()->bubble(rec_msg->getType());
               else this->getParentModule()->bubble(rec_msg->getTopic());
               getParentModule()->getDisplayString().setTagArg("r",0,"0");


           }
           else{
               cModule *sender=simulation.getModule(senderID);
               int x_sen=atoi(sender->getParentModule()->getDisplayString().getTagArg("p",0));
               int y_sen=atoi(sender->getParentModule()->getDisplayString().getTagArg("p",1));

               double d=sqrt(pow(x_cor-x_sen,2)+pow(y_cor-y_sen,2));
               double PL = PLd0+10*pathLossExponent*log(d/d0)+normal(0,sigma);//PL=PLd0+10*pathLossExponent*log(d/d0)+Xg



               if(rec_sensitivity>rec_msg->getTx_power()+2*antenna_gain-PL){
                   simulation.getModule(senderID)->getParentModule()->bubble("packet lost");
                   com_loss++;

               }
               else{

                  send(rec_msg,"node_io$o");
                  Battery+=55*tx_rx_time;
               }
           }
    }
    else if(strcmp(msg->getClassName(),"Tree")==0)
    {
        Tree *rec_msg = check_and_cast<Tree *>(msg);

        int senderID=rec_msg->getSenderModule()->getId();
        if(senderID==neighbourID){
            if(!rec_msg->getBroadcast()){
                cModule *receiver=simulation.getModule(rec_msg->getDestinationID());

                int x_rec=atoi(receiver->getParentModule()->getDisplayString().getTagArg("p",0));
                int y_rec=atoi(receiver->getParentModule()->getDisplayString().getTagArg("p",1));
                int distance=sqrt(pow(x_rec-x_cor,2)+pow(y_rec-y_cor,2));


                if (distance<95.35){
                    Tx_power=10;
                    i_tx=137;
                }
                else if(distance<108.76){
                    Tx_power=12;
                    i_tx=155;
                }
                else if(distance<124.05){
                    Tx_power=14;
                    i_tx=170;
                }
                else if(distance<141.50){
                    Tx_power=16;
                    i_tx=188;
                }
                else{
                    Tx_power=18;
                    i_tx=215;
                }
                rec_msg->setTx_power(Tx_power);

                getParentModule()->getDisplayString().setTagArg("r",0,"200");
                sendDirect(rec_msg,receiver,"wireless");
                if(!strcmp(rec_msg->getTopic(),"NULL")) this->getParentModule()->bubble(rec_msg->getType());
                else this->getParentModule()->bubble(rec_msg->getTopic());
                getParentModule()->getDisplayString().setTagArg("r",0,"0");
            }

            else{
                int num_nodes=this->getAncestorPar("No_nodes");

                for(int i=0;i<num_nodes;i++){
                    if(this->getParentModule()->getIndex()!=i){
                        char buf[50];
                        sprintf(buf,"node[%d].zigbee",i);
                        cModule *receiver=simulation.getModuleByPath(buf);
                        int x_receiver=atoi(receiver->getParentModule()->getDisplayString().getTagArg("p",0));
                        int y_receiver=atoi(receiver->getParentModule()->getDisplayString().getTagArg("p",1));
                        int rec_dis=sqrt(pow(x_receiver-x_cor,2)+pow(y_receiver-y_cor,2));
                        if(rec_dis<200){
                            rec_msg->setDestinationID(receiver->getId());
                            rec_msg->setTx_power(18);
                            getParentModule()->getDisplayString().setTagArg("r",0,"200");
                            sendDirect(rec_msg->dup(),receiver,"wireless");
                            if(!strcmp(rec_msg->getTopic(),"NULL")) this->getParentModule()->bubble(rec_msg->getType());
                            else this->getParentModule()->bubble(rec_msg->getTopic());
                            getParentModule()->getDisplayString().setTagArg("r",0,"0");
                        }
                    }
                }
                i_tx=215;
            }
            Battery+=i_tx*tx_rx_time;
        }
    else{
        cModule *sender=simulation.getModule(senderID);
        int x_sen=atoi(sender->getParentModule()->getDisplayString().getTagArg("p",0));
        int y_sen=atoi(sender->getParentModule()->getDisplayString().getTagArg("p",1));

        double d=sqrt(pow(x_cor-x_sen,2)+pow(y_cor-y_sen,2));
        double PL = PLd0+10*pathLossExponent*log(d/d0)+normal(0,sigma);//PL=PLd0+10*pathLossExponent*log(d/d0)+Xg



        if(rec_sensitivity>rec_msg->getTx_power()+2*antenna_gain-PL){
            simulation.getModule(senderID)->getParentModule()->bubble("packet lost");
            com_loss++;

        }
        else{

           send(rec_msg,"node_io$o");
           Battery+=55*tx_rx_time;
        }
    }
    }

    BatteryUsage.record(Battery);
}

void Zigbee::finish(){



       recordScalar("#com_loss",com_loss);////////////


}





